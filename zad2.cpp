//
// Created by bartek on 27.11.19.
//

#include <iostream>

class A {
protected:
    int a;
public:
    A (int pa){a = pa;}
    virtual void mut1() {
        a += 1;
    }
    int mut2(){
        this->mut1();
        return a;
    }
    virtual int ob(){
        return a+1;
    }
};

class B : public A {
public:
    B(int pb) : A(pb){
        a+=2;
    }
    void mut1() override {
        a += 3;
    }
    int ob() override {
        return a + 2;
    }
};

class D : public A {
public:
    D(int pc):A(pc) {}
    int ob2() {
        return 0;
    }

};

int main(){
    A a1(2);
    B b1(3);
    D d1(4);
    A *ap1 = &b1;
    A a2(1);
    a2 = *ap1;
    A *ap2 = &b1;
    B *bp1 = dynamic_cast<B*>(ap1);
    A *ap3 = &d1;

    a1.mut1();
    int p = a1.mut2();
    int q = a1.ob();
    int r = b1.mut2();
    int s = ap1->ob();
    int t = ap1->mut2();
    int u = a2.mut2();
    int w = 0;
    if (sizeof(*ap1) == sizeof(b1)) w=1;
    int v = bp1->mut2();
    int x = ap3->ob();
    std::cout << "end";
}