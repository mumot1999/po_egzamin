//
// Created by bartek on 27.11.19.
//

#include <iostream>

template <class T>
class ElementListy{
public:
    virtual void setNext(ElementListy* elementListy){}

    virtual ElementListy* getNext(){}

    virtual T getValue(){}
};

template <class T>
class MyElement : public ElementListy<T> {
private:
    T value;
    MyElement* next;
public:
    MyElement(T value){
        this->value = value;
    }
    void setNext(ElementListy<T>* element){
        this->next = dynamic_cast<MyElement*>(element);
    }

    ElementListy<T>* getNext(){
        return this->next;
    }

    T getValue(){
        return this->value;
    }
};

template <class X>
class List{
private:
    ElementListy<X> *first;
    ElementListy<X> *last;
    ElementListy<X> *current;
    
public:
    List(ElementListy<X>* first){
        this->first = first;
        this->current = first;
        this->last = first;
    }

    void addElement(ElementListy<X> *element){
        this->last->setNext(element);
        this->last = element;
    }
    
    X getFirstValue(){
        return this->first->getValue();
    }
    
    X getNextValue(){
        ElementListy<X> *next = this->current->getNext();
        if(!next){
            throw std::runtime_error("EndOfList");
//            throw EndOfList;
        }
        return next->getValue();
    }

};

int main(){
    List<std::string> lista(new MyElement<std::string> ("Ala ma kota"));
    lista.addElement(new MyElement<std::string> ("Jarek ma psa"));

    std::string string = lista.getFirstValue();
    std::cout << string << std::endl;
    std::cout << lista.getNextValue();


}