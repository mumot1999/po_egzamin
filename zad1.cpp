#include <iostream>

class A {
protected:
    int a;
public:
    A (int pa){a = pa;}
    virtual void mut1(int m1) {
        a += m1;
    }
    int mut2(int m2){
        this->mut1(m2+1);
    }
    virtual int obs(){
        return a+1;
    }
};

class B : virtual public A {
public:
    B(int pb) : A(2*pb){
        a+=1;
    }
    void mut1(int m1) override {
        a -= m1;
    }
    int obs() override {
        return (A::obs() + a + 2);
    }
};

class C : public A {
public:
    C(int pc):A(pc+1) {}
    int obs2() {
        return a;
    }

};

int main() {
    A a1(1);
    A a2(0);
    B b1(2);
    C c1(3);
    A* ap1 = &b1;
    A* ap3 = &c1;
    a2 = *ap1;
    B* bp1 = dynamic_cast<B*>(ap1);

    int p;
    b1.mut2(2);
    p=b1.obs();

    int q;
    ap1->mut1(1);
    q = ap1->obs();

    int u;
    ap1->mut2(2);
    u = ap1->obs();

    int r;
    bp1->mut1(1);
    r = bp1->obs();

    int s;
    a2.mut2(2);
    s=a2.obs();


    int y = 1;
    if(sizeof(*ap1) ==  sizeof(b1))
        y = 0;

    std::cout << y;

}